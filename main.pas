unit main;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, Forms, Controls, Graphics, Dialogs, StdCtrls, ExtCtrls,
  ActnList;

type

  { TMainForm }

  TMainForm = class(TForm)
    WealthLabel: TLabel;
    NextTurnButton: TButton;
    procedure FormCreate(Sender: TObject);
    procedure NextTurnButtonClick(Sender: TObject);
  private
    CurrentWealth: integer;
    procedure UpdateWealthLabel;
    procedure IncrementWealth;
  public

  end;

var
  MainForm: TMainForm;

implementation

{$R *.lfm}

{ TMainForm }

procedure TMainForm.FormCreate(Sender: TObject);
begin
  CurrentWealth:=0;
  UpdateWealthLabel
end;

procedure TMainForm.NextTurnButtonClick(Sender: TObject);
begin
  IncrementWealth;
  UpdateWealthLabel;
end;

procedure TMainForm.UpdateWealthLabel;
begin
  WealthLabel.Caption:=Format('Ресурсы: %d', [CurrentWealth]);
end;

procedure TMainForm.IncrementWealth;
begin
  CurrentWealth:=CurrentWealth + 1;
end;

end.

